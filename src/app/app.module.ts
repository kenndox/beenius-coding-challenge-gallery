import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { UserCellComponent } from './components/user-cell/user-cell.component';
import { AlbumCellComponent } from './components/album-cell/album-cell.component';
import { PhotoComponent } from './components/photo/photo.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { AlbumListComponent } from './components/album-list/album-list.component';
import { PhotoListComponent } from './components/photo-list/photo-list.component';
import { PhotoCellComponent } from './components/photo-cell/photo-cell.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    BreadcrumbsComponent,
    UserCellComponent,
    AlbumCellComponent,
    PhotoComponent,
    UserListComponent,
    AlbumListComponent,
    PhotoListComponent,
    PhotoCellComponent,
  ],
  imports: [CommonModule, BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
