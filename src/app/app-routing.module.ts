import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumListComponent } from './components/album-list/album-list.component';
import { PhotoListComponent } from './components/photo-list/photo-list.component';
import { UserListComponent } from './components/user-list/user-list.component';

const routes: Routes = [
  {
    path: '',
    data: { breadcrumb: 'Users' },
    children: [
      {
        path: '',
        pathMatch: 'full',
        data: { breadcrumb: null },
        component: UserListComponent
      },
      {
        path: 'user/:userId',
        data: { breadcrumb: null },
        children: [
          {
            path: '',
            data: { breadcrumb: 'Albums' },
            children: [
              {
                path: '',
                data: { breadcrumb: null },
                component: AlbumListComponent,
              },
              {
                path: 'album/:albumId',
                data: { breadcrumb: null },
                children: [
                  {
                    path: '',
                    data: { breadcrumb: 'Photos' },
                    component: PhotoListComponent,
                  },
                ]
              },
            ]
          },
          
        ]
      },
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {paramsInheritanceStrategy: 'always'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
