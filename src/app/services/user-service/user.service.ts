import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  baseUrl = 'https://jsonplaceholder.typicode.com';

  constructor(private http: HttpClient) {}

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.baseUrl}/users`);
  }

  getUser(userId: number): Observable<User | null> {
    if (!userId) {
      return of(null);
    }
    return this.http
      .get<User[]>(`${this.baseUrl}/users?id=${userId}`)
      .pipe(map((res) => (res.length ? res[0] : null)));
  }
}
