import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Album, Photo } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AlbumService {
  baseUrl = 'https://jsonplaceholder.typicode.com';

  constructor(protected http: HttpClient) {}

  getAlbums(userId: number): Observable<Album[]> {
    if (!userId) {
      return of([]);
    }
    return this.http.get<Album[]>(`${this.baseUrl}/users/${userId}/albums`);
  }

  getAlbum(albumId: number): Observable<Album | null> {
    if (!albumId) {
      return of(null);
    }
    return this.http
      .get<Album[]>(`${this.baseUrl}/albums?id=${albumId}`)
      .pipe(map((res) => (res.length ? res[0] : null)));
  }

  getAlbumPhotos(albumId: number): Observable<Photo[]> {
    if (!albumId) {
      return of([]);
    }
    return this.http.get<Photo[]>(`${this.baseUrl}/albums/${albumId}/photos`);
  }
}
