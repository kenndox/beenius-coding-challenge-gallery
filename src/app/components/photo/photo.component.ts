import { Component, Input, OnInit } from '@angular/core';
import { Photo } from 'src/app/models/user.model';
import { AlbumService } from 'src/app/services/photo-service/album.service';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss', '../../shared/shared-styles.scss'],
})
export class PhotoComponent implements OnInit {
  @Input() photo: Photo;

  photoLoaded = false;

  constructor(protected albumService: AlbumService) {}

  ngOnInit(): void {}
}
