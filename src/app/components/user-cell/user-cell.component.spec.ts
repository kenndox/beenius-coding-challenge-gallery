import {
  HttpClient,
  HttpClientModule,
  HttpHandler,
} from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { User } from 'src/app/models/user.model';
import { PhotoComponent } from '../photo/photo.component';

import { UserCellComponent } from './user-cell.component';

describe('UserCellComponent', () => {
  let component: UserCellComponent;
  let fixture: ComponentFixture<UserCellComponent>;
  let mockUser: User;
  let overlay: HTMLElement;

  beforeEach(async () => {
    mockUser = {
      id: 1,
      name: 'Mock User',
      username: 'mockingbird',
    };

    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [UserCellComponent, PhotoComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCellComponent);
    component = fixture.componentInstance;
    component.user = mockUser;
    fixture.detectChanges();
    overlay = fixture.nativeElement.querySelector('.cell-overlay');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display overlay if user is present', () => {
    fixture.detectChanges();
    expect(overlay).toBeDefined();
  });

  it('should not display overlay if user is not present', () => {
    component.user = null;
    fixture.detectChanges();
    overlay = fixture.nativeElement.querySelector('.cell-overlay');
    fixture.detectChanges();
    expect(overlay).toBeNull();
  });

  it("should display user's name and username in the overlay", () => {
    fixture.detectChanges();
    const userNameElement = overlay.querySelector('.user-name');
    const userUsernameElement = overlay.querySelector('.user-username');
    expect(userNameElement.innerHTML).toContain(mockUser.name);
    expect(userUsernameElement.innerHTML).toContain(mockUser.username);
  });
});
