import { Component, Input, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Album, Photo, User } from 'src/app/models/user.model';
import { AlbumService } from 'src/app/services/photo-service/album.service';

@Component({
  selector: 'app-user-cell',
  templateUrl: './user-cell.component.html',
  styleUrls: ['./user-cell.component.scss', '../../shared/shared-styles.scss'],
})
export class UserCellComponent implements OnInit {
  @Input() user: User;

  albumPhoto$: Observable<Photo | null>;

  constructor(protected albumService: AlbumService) {}

  ngOnInit(): void {
    this.albumPhoto$ = this.getRandomAlbumPhoto();
  }

  getRandomAlbumPhoto(): Observable<Photo | null> {
    if (!this.user) {
      return of(null);
    }
    return this.albumService.getAlbums(this.user.id).pipe(
      switchMap((albums: Album[]) => {
        const randomAlbumId =
          albums[Math.floor(Math.random() * albums.length)].id;
        return this.albumService.getAlbumPhotos(randomAlbumId).pipe(
          map((photos: Photo[]) => {
            return photos[Math.floor(Math.random() * photos.length)];
          })
        );
      })
    );
  }
}
