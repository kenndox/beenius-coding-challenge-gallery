import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { Album, Photo, User } from 'src/app/models/user.model';
import { AlbumService } from 'src/app/services/photo-service/album.service';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.scss', '../../shared/shared-styles.scss'],
})
export class AlbumListComponent implements OnInit {
  loadingRequest$: Observable<any>;
  albums$: Observable<Album[]>;
  user$: Observable<User | null>;

  albums: Album[];
  user: User | null;

  constructor(
    private route: ActivatedRoute,
    protected albumService: AlbumService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    const userId = +this.route.snapshot.paramMap.get('userId');
    this.user$ = this.userService.getUser(userId);
    this.albums$ = this.albumService.getAlbums(userId);
    this.loadingRequest$ = combineLatest([this.user$, this.albums$]).pipe(
      tap((res) => {
        this.user = res[0];
        this.albums = res[1];
      })
    );
  }
}
