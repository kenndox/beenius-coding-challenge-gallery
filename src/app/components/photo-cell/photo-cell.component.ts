import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Album, Photo, User } from 'src/app/models/user.model';
import { AlbumService } from 'src/app/services/photo-service/album.service';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-photo-cell',
  templateUrl: './photo-cell.component.html',
  styleUrls: ['./photo-cell.component.scss', '../../shared/shared-styles.scss'],
})
export class PhotoCellComponent implements OnInit {
  @Input() photo: Photo;

  @Input() displayOverlay = true;

  loadingRequest$: Observable<any>;
  album$: Observable<Album | null>;
  user$: Observable<User | null>;

  user: User | null;
  album: Album | null;
  photoLoaded = false;
  fullScreenOpen = false;
  detailsOpen = false;

  constructor(
    protected route: ActivatedRoute,
    protected albumService: AlbumService,
    protected userService: UserService
  ) {}

  ngOnInit(): void {
    const userId = +this.route.snapshot.paramMap.get('userId');
    const albumId = this.photo?.albumId;
    this.user$ = this.userService.getUser(userId);
    this.album$ = this.albumService.getAlbum(albumId);
    this.loadingRequest$ = combineLatest([this.user$, this.album$]).pipe(
      tap((res) => {
        this.user = res[0];
        this.album = res[1];
      })
    );
  }
}
