import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { combineLatest, Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Album, Photo, User } from 'src/app/models/user.model';
import { AlbumService } from 'src/app/services/photo-service/album.service';
import { UserService } from 'src/app/services/user-service/user.service';
import { PhotoComponent } from '../photo/photo.component';

import { PhotoCellComponent } from './photo-cell.component';

describe('PhotoCellComponent', () => {
  let component: PhotoCellComponent;
  let fixture: ComponentFixture<PhotoCellComponent>;
  let overlay: HTMLElement;
  let mockUser: User;
  let mockAlbum: Album;
  let mockPhoto: Photo;
  let getUserSpy: any;
  let getAlbumSpy: any;

  beforeEach(async () => {
    mockUser = {
      id: 1,
      name: 'Mock User',
      username: 'mockingbird',
    };

    mockAlbum = {
      userId: 1,
      id: 1,
      title: 'Mock album',
    };

    mockPhoto = {
      id: 1,
      albumId: 1,
      title: 'Mock photo',
      url: 'http://google.com',
      thumbnailUrl: 'http://google.com',
    };

    const mockUserService = jasmine.createSpyObj('UserService', ['getUser']);
    const mockAlbumService = jasmine.createSpyObj('AlbumService', ['getAlbum']);

    getUserSpy = mockUserService.getUser.and.returnValue(of(mockUser));
    getAlbumSpy = mockAlbumService.getAlbum.and.returnValue(of(mockAlbum));

    await TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([])],
      declarations: [PhotoCellComponent, PhotoComponent],
      providers: [
        { provide: UserService, useValue: mockUserService },
        { provide: AlbumService, useValue: mockAlbumService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoCellComponent);
    component = fixture.componentInstance;
    component.photo = mockPhoto;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display overlay by default', () => {
    fixture.detectChanges();
    overlay = fixture.nativeElement.querySelector('.cell-overlay');
    expect(overlay.innerHTML).toContain(mockPhoto.title);
  });

  it('should not display overlay if displayOverlay is false', () => {
    component.displayOverlay = false;
    fixture.detectChanges();
    overlay = fixture.nativeElement.querySelector('.cell-overlay');
    expect(overlay).toBeNull();
  });

  it('should display the title of the input photo in the overlay', () => {
    fixture.detectChanges();
    overlay = fixture.nativeElement.querySelector('.cell-overlay');
    expect(overlay.innerHTML).toContain(mockPhoto.title);
  });

  it('should open a fullscreen photo on click of image', () => {
    fixture.detectChanges();
    const imageCell = fixture.nativeElement.querySelector('.image-cell');
    imageCell.click();
    const fullScreenView =
      fixture.nativeElement.querySelector('.fullscreen-view');
    expect(component.fullScreenOpen).toBeTrue();
    expect(fullScreenView).toBeDefined();
  });

  it('should close fullscreen view when pressing close button', () => {
    fixture.detectChanges();
    const imageCell = fixture.nativeElement.querySelector('.image-cell');
    imageCell.click();
    fixture.detectChanges();
    let fullScreenView =
      fixture.nativeElement.querySelector('.fullscreen-view');
    const closeButton = fullScreenView.querySelector('.close-button');
    closeButton.click();
    fixture.detectChanges();
    fullScreenView = fixture.nativeElement.querySelector('.fullscreen-view');
    expect(component.fullScreenOpen).toBeFalse();
    expect(fullScreenView).toBeNull();
  });

  it('should add .open to details-panel on fullscreen image click', () => {
    fixture.detectChanges();
    const imageCell = fixture.nativeElement.querySelector('.image-cell');
    imageCell.click();
    fixture.detectChanges();
    let fullScreenView =
      fixture.nativeElement.querySelector('.fullscreen-view');
    const image = fullScreenView.querySelector('img');
    expect(image).toBeDefined();
    image.click();
    fixture.detectChanges();
    const detailsPanel = fixture.nativeElement.querySelector('.details-panel');
    expect(detailsPanel).toHaveClass('open');
  });

  it('should remove .open from details-panel on fullscreen image click while it is open', () => {
    fixture.detectChanges();
    const imageCell = fixture.nativeElement.querySelector('.image-cell');
    imageCell.click();
    fixture.detectChanges();
    let fullScreenView =
      fixture.nativeElement.querySelector('.fullscreen-view');
    const image = fullScreenView.querySelector('img');
    expect(image).toBeDefined();
    image.click();
    fixture.detectChanges();
    image.click();
    fixture.detectChanges();
    const detailsPanel = fixture.nativeElement.querySelector('.details-panel');
    expect(detailsPanel).not.toHaveClass('open');
  });

  it('should display photo title, album title and user name in the details panel', () => {
    fixture.detectChanges();
    const imageCell = fixture.nativeElement.querySelector('.image-cell');
    imageCell.click();
    fixture.detectChanges();
    let fullScreenView =
      fixture.nativeElement.querySelector('.fullscreen-view');
    const image = fullScreenView.querySelector('img');
    expect(image).toBeDefined();
    image.click();
    fixture.detectChanges();
    const detailsPanel = fixture.nativeElement.querySelector('.details-panel');
    const photoTitle = detailsPanel.querySelector('.photo-title');
    expect(photoTitle.innerHTML).toContain(mockPhoto.title);
    const albumTitle = detailsPanel.querySelector('.album-title');
    expect(albumTitle.innerHTML).toContain(mockAlbum.title);
    const userName = detailsPanel.querySelector('.user-name');
    expect(userName.innerHTML).toContain(mockUser.name);
  });
});
