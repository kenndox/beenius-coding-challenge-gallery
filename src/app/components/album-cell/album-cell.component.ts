import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Album, Photo } from 'src/app/models/user.model';
import { AlbumService } from 'src/app/services/photo-service/album.service';

@Component({
  selector: 'app-album-cell',
  templateUrl: './album-cell.component.html',
  styleUrls: ['./album-cell.component.scss', '../../shared/shared-styles.scss'],
})
export class AlbumCellComponent implements OnInit {
  @Input() album: Album;

  photos$: Observable<Photo[]>;

  constructor(protected albumService: AlbumService) {}

  ngOnInit(): void {
    this.photos$ = this.albumService.getAlbumPhotos(this.album?.id).pipe(
      map((photos: Photo[]) => {
        return this.getRandomPhotos(photos, 4);
      })
    );
  }

  getRandomPhotos(photos: Photo[], count: number): Photo[] {
    let randomPhotos = [];
    let counter = count;
    while (photos.length && counter > 0) {
      randomPhotos.push(
        photos.splice(Math.floor(Math.random() * photos.length), 1)?.[0]
      );
      --counter;
    }
    return randomPhotos;
  }
}
