import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Album, Photo } from 'src/app/models/user.model';
import { AlbumService } from 'src/app/services/photo-service/album.service';
import { PhotoComponent } from '../photo/photo.component';

import { AlbumCellComponent } from './album-cell.component';

describe('AlbumCellComponent', () => {
  let component: AlbumCellComponent;
  let fixture: ComponentFixture<AlbumCellComponent>;
  let mockAlbum: Album;
  let getAlbumSpy: any;
  let overlay: HTMLElement;

  beforeEach(async () => {
    mockAlbum = {
      userId: 1,
      id: 1,
      title: 'Mock Album',
    };
    const mockAlbumPhotos: Photo[] = [
      {
        id: 1,
        albumId: 1,
        title: 'Mock Photo 1',
        url: 'http://google.com',
        thumbnailUrl: 'http://google.com',
      },
      {
        id: 2,
        albumId: 1,
        title: 'Mock Photo 2',
        url: 'http://google.com',
        thumbnailUrl: 'http://google.com',
      },
      {
        id: 3,
        albumId: 1,
        title: 'Mock Photo 3',
        url: 'http://google.com',
        thumbnailUrl: 'http://google.com',
      },
      {
        id: 4,
        albumId: 1,
        title: 'Mock Photo 4',
        url: 'http://google.com',
        thumbnailUrl: 'http://google.com',
      },
      {
        id: 5,
        albumId: 1,
        title: 'Mock Photo 5',
        url: 'http://google.com',
        thumbnailUrl: 'http://google.com',
      },
      {
        id: 6,
        albumId: 1,
        title: 'Mock Photo 6',
        url: 'http://google.com',
        thumbnailUrl: 'http://google.com',
      },
    ];
    const mockAlbumService = jasmine.createSpyObj('AlbumService', [
      'getAlbumPhotos',
    ]);
    getAlbumSpy = mockAlbumService.getAlbumPhotos.and.returnValue(
      of(mockAlbumPhotos)
    );

    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [{ provide: AlbumService, useValue: mockAlbumService }],
      declarations: [AlbumCellComponent, PhotoComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumCellComponent);
    component = fixture.componentInstance;
    component.album = mockAlbum;
    fixture.detectChanges();
    overlay = fixture.nativeElement.querySelector('.cell-overlay');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display overlay if album is present', () => {
    fixture.detectChanges();
    expect(overlay).toBeTruthy();
  });

  it('should not display overlay if album is not present', () => {
    component.album = null;
    fixture.detectChanges();
    overlay = fixture.nativeElement.querySelector('.cell-overlay');
    expect(overlay).toBeNull();
  });

  it('should display album title in the overlay', () => {
    fixture.detectChanges();
    component.album = mockAlbum;
    expect(overlay.innerHTML).toContain(mockAlbum.title);
  });

  it('should display 4 random photos', () => {
    fixture.detectChanges();
    const thumbnails = fixture.nativeElement
      .querySelector('.thumbnails')
      .querySelectorAll('.thumbnail');
    fixture.detectChanges();
    expect(thumbnails.length).toEqual(4);
  });
});
