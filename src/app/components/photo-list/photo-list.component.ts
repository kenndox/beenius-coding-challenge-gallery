import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Album, Photo, User } from 'src/app/models/user.model';
import { AlbumService } from 'src/app/services/photo-service/album.service';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.scss', '../../shared/shared-styles.scss'],
})
export class PhotoListComponent implements OnInit {
  loadingRequest$: Observable<Array<any>>;
  photos$: Observable<Photo[]>;
  album$: Observable<Album | null>;
  user$: Observable<User | null>;

  photos: Photo[];
  album: Album | null;
  user: User | null;

  constructor(
    private route: ActivatedRoute,
    private albumService: AlbumService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    const userId = +this.route.snapshot.paramMap.get('userId');
    const albumId = +this.route.snapshot.paramMap.get('albumId');
    this.album$ = this.albumService.getAlbum(albumId);
    this.user$ = this.userService.getUser(userId);
    this.photos$ = this.albumService.getAlbumPhotos(albumId);
    this.loadingRequest$ = combineLatest([
      this.user$,
      this.album$,
      this.photos$,
    ]).pipe(
      tap((res) => {
        this.user = res[0];
        this.album = res[1];
        this.photos = res[2];
      })
    );
  }
}
