import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss'],
})
export class BreadcrumbsComponent implements OnInit {
  breadcrumbs: any[] = [];

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.router.events.subscribe((routerEvent) => {
      if (routerEvent instanceof NavigationEnd) {
        this.breadcrumbs = [];
        let currentRoute: any = this.route.root,
          url = '';
        do {
          let childrenRoutes = currentRoute.children;
          currentRoute = null;
          childrenRoutes.forEach((route: any) => {
            if (route.outlet === 'primary') {
              let routeSnapshot = route.snapshot;
              const url_paths = routeSnapshot.url.map(
                (segment: any) => segment.path
              );
              url += url_paths.join('/');
              if (url_paths.length) {
                url += '/';
              }
              this.breadcrumbs.push({
                label: route.snapshot.data.breadcrumb,
                url: url,
              });
              currentRoute = route;
            }
          });
        } while (currentRoute);
        this.breadcrumbs = this.breadcrumbs.filter(
          (breadcrumb) => breadcrumb.label
        );
      }
    });
  }
}
